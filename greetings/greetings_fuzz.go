//go:build gofuzz

package greetings

func Fuzz(data []byte) int {
	if _, err := Hello(string(data[:])); err != nil {
		return 1
	}
	return 0
}
