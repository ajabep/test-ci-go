package greetings

import (
	"fmt"
	"regexp"
	"strings"
	"testing"
	"time"

	fuzz "github.com/google/gofuzz"
)

const NbRandomTests = 100000
const NbRandomNameTests = 100000

func randomNotEmptyString(f *fuzz.Fuzzer) string {
	str := ""
	for str == "" {
		f.Fuzz(&str)
	}
	return str
}

// TestHelloName calls greetings.Hello with a name, checking
// for a valid return value.
func TestHelloName(t *testing.T) {
	name := "Gladys"
	want := regexp.MustCompile(`\b` + name + `\b`)
	msg, err := Hello(name)
	if !want.MatchString(msg) || err != nil {
		t.Fatalf(`Hello(%q) = %q, %v, want match for %#q, nil`, name, msg, err, want)
	}

	seed := time.Now().UnixNano()
	fmt.Printf("Seed for fuzzing = %v\n", seed)
	f := fuzz.NewWithSeed(seed)
	for i := 0; i < NbRandomTests; i++ {
		name := randomNotEmptyString(f)
		if len(name) == 0 {
			continue
		}
		msg, err = Hello(name)
		if err != nil {
			t.Fatalf(`Hello(%q) = %q, %v ; Unknown error!`, name, msg, err)
		} else if !(strings.Contains(msg, name) && len(msg) >= len(name)+2) {
			t.Fatalf(`Hello(%q) = %q, %v ; len(msg) = %v ; len(name) = %v ; Does not contains the name or not long enought`, name, msg, err, len(msg), len(name))
		}
	}
}

// TestHelloRandomName calls greetings.Hello with a name, checking
// that the message is really random
func TestHelloRandomName(t *testing.T) {
	name := "Gladys"
	prevmsg := ""
	var msg string
	var err error

	for i := 0; i < NbRandomNameTests; i++ {
		msg, err = Hello(name)
		if err != nil {
			t.Fatalf(`Hello(%q) = %q, %v ; Unknown error!`, name, msg, err)
		} else if prevmsg != msg {
			return
		}
	}

	t.Fatalf(`Hello("Gladys") is not random , the only message returned is %v`, msg)
}

// TestHelloEmpty calls greetings.Hello with an empty string,
// checking for an error.
func TestHelloEmpty(t *testing.T) {
	msg, err := Hello("")
	if msg != "" || err == nil {
		t.Fatalf(`Hello("") = %q, %v, want "", error`, msg, err)
	}
}

// TestHellosName calls greetings.Hellos with a some names, checking
// for a valid return value.
func TestHellosName(t *testing.T) {
	names := []string{
		"Gladys",
		"John",
		"Erica",
	}
	msgs, err := Hellos(names)
	if err != nil {
		t.Fatalf(`Hellos(%v) = %v, %v ; It fails, it should not`, names, msgs, err)
	}
	if len(names) != len(msgs) {
		t.Fatalf(`len(names) = %v ; len(msgs) = %v ; not the same length\n`, len(names), len(msgs))
	}
	for _, name := range names {
		want := regexp.MustCompile(`\b` + name + `\b`)
		if !want.MatchString(msgs[name]) {
			t.Fatalf(`Hellos(%v) = %v, %v ; @%v want match for %#q, nil`, names, msgs, err, name, want)
		}
	}

	seed := time.Now().UnixNano()
	fmt.Printf("Seed for fuzzing = %v\n", seed)
	f := fuzz.NewWithSeed(seed)
	for i := 0; i < NbRandomTests; i++ {
		var lennames int
		for lennames != 0 {
			f.Fuzz(&lennames)
		}

		names := make([]string, lennames)
		for j := 0; j < lennames; j++ {
			names[j] = randomNotEmptyString(f)
		}

		msgs, err := Hellos(names)
		if err != nil {
			t.Fatalf(`Hellos(%v) = %v, %v ; It fails, it should not`, names, msgs, err)
		}

		if len(names) != len(msgs) {
			t.Fatalf(`len(names) = %v ; len(msgs) = %v ; not the same length\n`, len(names), len(msgs))
		}
		for _, name := range names {
			want := regexp.MustCompile(`\b` + name + `\b`)
			if !want.MatchString(msgs[name]) {
				t.Fatalf(`Hellos(%v) = %v, %v ; @%v want match for %#q, nil`, names, msgs, err, name, want)
			}
		}
	}
}

// TestHellosRandomName calls greetings.Hellos with names, checking
// that the message is really random
func TestHellosRandomName(t *testing.T) {
	sameAcrossArray := true
	names := []string{
		"Gladys1",
		"Gladys2",
		"Gladys3",
	}
	msgs1, err := Hellos(names)
	if err != nil {
		t.Fatalf(`Hellos(%v) = %v, %v ; It fails, it should not`, names, msgs1, err)
	}
	if len(names) != len(msgs1) {
		t.Fatalf(`len(names) = %v ; len(msgs1) = %v ; not the same length\n`, len(names), len(msgs1))
	}

	for i := 0; i < NbRandomNameTests; i++ {
		msgs2, err2 := Hellos(names)
		if err2 != nil {
			t.Fatalf(`Hellos(%v) = %v, %v ; It fails, it should not`, names, msgs2, err2)
		}
		if len(names) != len(msgs2) {
			t.Fatalf(`len(names) = %v ; len(msgs2) = %v ; not the same length\n`, len(names), len(msgs2))
		}
		for _, name := range names {
			if msgs1[name] != msgs2[name] {
				sameAcrossArray = false
			}
		}
	}

	if sameAcrossArray {
		t.Fatalf(`Hello(%v) has the same output across arrays`, names)
	}
}

// TestHellosEmpty calls greetings.Hellos with an empty string,
// checking for an error.
func TestHellosEmpty(t *testing.T) {
	tests := [][]string{
		{"abc", "def", ""},
		{"abc", "", "ghi"},
		{"", "def", "ghi"},
		{"abc", "", ""},
		{"", "def", ""},
		{"", "", "ghi"},
		{"", "", ""},
	}
	for _, v := range tests {
		msg, err := Hellos(v)
		if msg != nil || err == nil {
			t.Fatalf(`Hellos(%q) = %q, %v, want nil, error`, v, msg, err)
		}
	}
}
